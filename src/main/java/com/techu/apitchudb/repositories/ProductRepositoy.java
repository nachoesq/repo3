package com.techu.apitchudb.repositories;

import com.techu.apitchudb.models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ProductRepositoy extends MongoRepository<ProductModel, String>{
}

