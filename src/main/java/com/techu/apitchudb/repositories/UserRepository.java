package com.techu.apitchudb.repositories;

import com.techu.apitchudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface UserRepository extends MongoRepository<UserModel, String> {
}
