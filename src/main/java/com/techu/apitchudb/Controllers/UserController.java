package com.techu.apitchudb.Controllers;


import com.techu.apitchudb.models.UserModel;
import com.techu.apitchudb.services.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")

public class UserController {

    @Autowired
    UserServices userService;

//**************************************************************************************************************
//                                       OBTENER USUARIOS
//**************************************************************************************************************

    @GetMapping("/users")

    public ResponseEntity<List<UserModel>> getusers(
            @RequestParam(name = "$orderby", required = false) String orderby
        ){
        System.out.println("<<<<<< getUsers >>>>>>");
        System.out.println("El valor de $orderby es " + orderby);


        return new ResponseEntity<>(
                this.userService.findAll(orderby)
                , HttpStatus.OK
        );


    }

//**************************************************************************************************************
//                                       CONSULTAR PRODUCTO POR SU ID
//**************************************************************************************************************

    @GetMapping("/users/{id}")
    public ResponseEntity <Object>  getUserById(@PathVariable String id){
        System.out.println("<<<<<< getUserById >>>>>>");
        System.out.println("La id del usuario es:  " + id);
        Optional<UserModel> result = this.userService.findById(id);


        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );

    }

//**************************************************************************************************************
//                                       AGREGAR USUARIOS
//**************************************************************************************************************

    @PostMapping("/users")
    //public ResponseEntity<UserModel> addProduct(@RequestBody UserModel user){
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("<<<<<< addUser >>>>>>");
        System.out.println("El nombre del usuario que se va a crear es " + user.getName());
        System.out.println("La edad del usuario que se va a crear es " + user.getAge());
        return new ResponseEntity<>(
                this.userService.add(user)
                , HttpStatus.CREATED
        );
    }

//**************************************************************************************************************
//                                       ACTUALIZAR UN USUARIO
//**************************************************************************************************************


    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("<<<<<<  updateUser >>>>>>");
        System.out.println("La id del usuario a acutalizar en parapámetro URL es: " + id);
        System.out.println("El nombre del usuario a acutalizar en paraámetro URL es: " + user.getName());
        System.out.println("La edad del usuario  a acutalizar es: " + user.getAge());

        Optional<UserModel> userToUpdate  = this.userService.findById(id);
        if (userToUpdate.isPresent()){
            System.out.println("OKOK Usuario para actualizar encontrado");
            this.userService.update(user);

        }

        return new ResponseEntity<>(
                user
                , userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND


        );
    }


//**************************************************************************************************
//                                                    BORRADO POR ID DE USAURIOS
//**************************************************************************************************

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("<<<<< deleteUser >>>>>");

        boolean deleteUser = this.userService.delete(id);
        return new ResponseEntity<>(
                deleteUser ? "OKOK, Usuario Borrado" : "XXXX usuario no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

} //Fin de la clase




