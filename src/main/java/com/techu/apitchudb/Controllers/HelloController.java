package com.techu.apitchudb.Controllers;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class HelloController {

    @RequestMapping("/")

    public String Index(){
        return "Hola mundo desde API TEch U con base de datos!";
    }

//    @RequestMapping("/hello")
//    public String hello(
//) {
//        return hello();
//    }

    @RequestMapping("/hello")
    public String hello(
            @RequestParam(value = "name", defaultValue = "Tech U") String name
    ){
        return String.format("Hola %s!" + name);
    }
}



