package com.techu.apitchudb.Controllers;

import com.techu.apitchudb.models.ProductModel;
import com.techu.apitchudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;


//**************************************************************************************************************
//                                       OBTENER PRODUCTOS
//**************************************************************************************************************


    @GetMapping("/products")

    public ResponseEntity<List<ProductModel>> getproducts(){
        System.out.println("<<<<<< getProducts >>>>>>");

        return new ResponseEntity<>(
                this.productService.findAll()
                , HttpStatus.OK
        );
    }

//    public List<ProductModel> getProducts(){
//        System.out.println("getProducts");
//
//        return this.productService.findAll();
//    }



//**************************************************************************************************************
//                                       CONSULTAR PRODUCTO POR SU ID
//**************************************************************************************************************

    @GetMapping("/products/{id}")
    public ResponseEntity <Object>  getProductById(@PathVariable String id){
    System.out.println("<<<<<< getProductId >>>>>>");
    System.out.println("La id del producto es " + id);
    Optional<ProductModel> result = this.productService.findById(id);


    return new ResponseEntity<>(
            result.isPresent() ? result.get() : "Producto no encontrado",
            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
            //new ProductModel(),
            //HttpStatus.OK
    );

    //x = (condicion) ? vale_esto_si:true : vale_esto_si_false


    }

//**************************************************************************************************************
//                                       AGREGAR PRODUCTOS
//**************************************************************************************************************

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("<<<<<< addProduct >>>>>>");
        System.out.println("La id del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio  del producto que se va a crear es " + product.getPrice());
        return new ResponseEntity<>(
            this.productService.add(product)
                //new ProductModel()
            , HttpStatus.CREATED
        );
    }

//**************************************************************************************************************
//                                       ACTUALIZAR UN PRODUCTO
//**************************************************************************************************************


        @PutMapping("/products/{id}")
        public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id){
        System.out.println("<<<<<<  UpdateProduct >>>>>>");
        System.out.println("La id del producto a acutalizar en parapámetro URL es: " + id);
        System.out.println("La descripción del producto a acutalizar en paraámetro URL es: " + product.getDesc());
        System.out.println("El precio  del producto a acutalizar es: " + product.getPrice());

            Optional<ProductModel> productToUpdate  = this.productService.findById(id);
            if (productToUpdate.isPresent()){
                System.out.println("Producto para actualizar encontrado");
                this.productService.update(product);

            }

            return new ResponseEntity<>(
                    product
                    , productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND


            );
        }

//**************************************************************************************************
//                                                      BORRADO POR ID
//**************************************************************************************************

        @DeleteMapping("/product/{id}")
        public ResponseEntity<String> deleteProduct(@PathVariable String id){
            System.out.println("<<<<< deleteProduct >>>>>");

            boolean deleteProduct = this.productService.delete(id);
            return new ResponseEntity<>(
                deleteProduct ? "Producto Borrado" : "Producto no borrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
            );
        }

}
