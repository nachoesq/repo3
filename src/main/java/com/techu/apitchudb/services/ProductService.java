package com.techu.apitchudb.services;

import com.techu.apitchudb.models.ProductModel;
import com.techu.apitchudb.repositories.ProductRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepositoy productRepository;

//**************************************************************************************************************
//                                       SERVICIO PARA LISTAR PRODUCTOS
//**************************************************************************************************************


    public List<ProductModel> findAll(){
        System.out.println("findAll ProductService");

        return this.productRepository.findAll();
    }

//**************************************************************************************************************
//                                       SERVICIO PARA AGREGAR PRODUCTOS
//**************************************************************************************************************

    public ProductModel add(ProductModel product){
        System.out.println("add ProductService");

        return this.productRepository.save(product);
    }


//**************************************************************************************************************
//                               SERVICIO PARA BUSCAR  PRODUCTOS POR IDENTIFICADOR
//**************************************************************************************************************

    public Optional<ProductModel> findById(String id){
        System.out.println("En findById de ProductService");

        return this.productRepository.findById(id);
    }

//**************************************************************************************************************
//                                       SERVICIO PARA ACTUALIZAR
//**************************************************************************************************************

    public ProductModel update(ProductModel productModel){

        System.out.println("Update en productService");
        return this.productRepository.save(productModel);
    }

//**************************************************************************************************************
//                                       SERVICIO PARA BORRAR PRODUCTOS
//**************************************************************************************************************


    public boolean delete(String id){
        System.out.println("Delete en productService");
        boolean result = false;

        if(this.findById(id).isPresent() == true){
            System.out.println("Producto encontrado, borrando...");
            this.productRepository.deleteById(id);
            result = true;

        }
        return result;

        // es una opción, peor no tenemnos el contro  en prodctRepository this.productRepository.findById()
        // no es un único punto de acceso, nos implica tocar más puntos creando más acoplamiento

    }
}
