package com.techu.apitchudb.services;

import com.techu.apitchudb.models.UserModel;
import com.techu.apitchudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class UserServices {
    @Autowired
    UserRepository userRepositoy;
//**************************************************************************************************************
//                                       SERVICIO PARA LISTAR USUARIOS
//**************************************************************************************************************


    public List<UserModel> findAll(String orderby){
        System.out.println("findAll service");

        if (orderby != null) {
            System.out.println("SE ah solicitado CON ordenación");
            return this.userRepositoy.findAll(Sort.by(Sort.Direction.ASC, "age"));
        }   else {
            System.out.println("Se ha solicitado SIN ordenación");
              return this.userRepositoy.findAll();
           }
    }





//**************************************************************************************************************
//                                       SERVICIO PARA AGREGAR USUARIOS
//**************************************************************************************************************

    public UserModel add(UserModel user){
        System.out.println("add UserService");

        return this.userRepositoy.save(user);
    }


//**************************************************************************************************************
//                               SERVICIO PARA BUSCAR  USUARIOS POR IDENTIFICADOR
//**************************************************************************************************************

    public Optional<UserModel> findById(String id){
        System.out.println("En findById de UserService");

        return this.userRepositoy.findById(id);
    }


//**************************************************************************************************************
//                                       SERVICIO PARA ACTUALIZAR USUARIOS
//**************************************************************************************************************

    public UserModel update(UserModel userModel){

        System.out.println("Update en userService");
        return this.userRepositoy.save(userModel);
    }

//**************************************************************************************************************
//                                       SERVICIO PARA BORRAR USUARIOS
//**************************************************************************************************************

    public boolean delete(String id){
        System.out.println("Delete en userService");
        boolean result = false;

        if(this.findById(id).isPresent() == true){
            System.out.println("OKOK Usuario encontrado, borrando...");
            this.userRepositoy.deleteById(id);
            result = true;

        } else {
            System.out.println("XXXX Usuario no encontrado");
        }
        return result;

    }
}
